import Vue from 'vue';
import Router from 'vue-router';
import SignInForm from './components/SignInForm.vue';
import Profile from './components/Profile.vue';
import Results from './components/Results.vue';
import FAQ from './components/FAQ.vue';
import ResultDetails from './components/ResultDetails.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'signin',
      component: SignInForm,
    },
    {
      path: '/faq',
      name: 'faq',
      component: FAQ,
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
    },
    {
      path: '/results',
      name: 'result',
      component: Results,
    },
    {
      path: '/resultdetails',
      name: 'resultdetails',
      component: ResultDetails,
    },
  ],
});
